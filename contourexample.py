import numpy as np
from astropy import units as u
from astropy.coordinates import SkyCoord
from astropy.io import fits
from astropy import wcs
import matplotlib.pyplot as plt
import aplpy
import matplotlib


def vlasscontour():

    fig = plt.figure(facecolor='w', edgecolor='w', frameon=True, figsize=(10.8,6))
    fitsfile='VLASS1.1.ql.T07t02.J011047-133000.10.2048.v1.I.iter1.image.pbcor.tt0.subim.fits'
    ra =   '01h11m09.09s'
    dec =  '-13d57m38.83s'
    size = 3.0/60.0

    c = SkyCoord(ra, dec, frame='icrs') 
    RA = c.ra.value  #Decimal degrees
    DEC = c.dec.value #Decimal degrees
    imdata = fits.getdata(fitsfile)[0,0,:,:]
    rms = np.std(imdata[1600:1700,220:320])
    
    ax1 = aplpy.FITSFigure(fitsfile, dimensions=[0,1], figure=fig, subplot=[0.15,0.1,0.50,0.9])
    ax1.show_colorscale(cmap='inferno', vmin=rms, vmax=10.0*rms, interpolation='nearest')
    ax1.set_tick_labels_format(xformat='hh:mm:ss', yformat='dd:mm')
    #ax1.show_colorscale(cmap='inferno', interpolation='nearest')
    ax1.show_rectangles(RA, DEC, size,size, edgecolor='white', facecolor='none', lw=2)  # Needs to be in degrees
    
    ax2 = aplpy.FITSFigure('T07t02_dss2blue.fits', figure=fig, subplot=[0.75,0.6,0.20,0.36])
    ax2.show_colorscale(cmap='bone', interpolation='nearest')
    ax2.set_tick_labels_format(xformat='hh:mm:ss', yformat='dd:mm')
    ax2.set_tick_labels_font(size='x-small')
    ax2.set_axis_labels_font(size='x-small')
    
    ax2.show_contour(fitsfile, levels=np.array([10.0,20.0,30.0,40.0,50.0,60.0])*rms, colors='orange', lw=0.5)
    

    plt.show()
    
  
vlasscontour()