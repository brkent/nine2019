import numpy as np
from astropy import units as u
from astropy.coordinates import SkyCoord
from astropy.io import fits
from astropy import wcs
import matplotlib.pyplot as plt
import aplpy
import matplotlib


fitsfile='VLASS1.1.ql.T07t02.J011047-133000.10.2048.v1.I.iter1.image.pbcor.tt0.subim.fits'

ra =   '01h11m09.09s'
dec =  '-13d57m38.83s'
size = 3.0/60.0

c = SkyCoord(ra, dec, frame='icrs') 
RA = c.ra.value
DEC = c.dec.value

fig = plt.figure(facecolor='w', edgecolor='w', frameon=True, figsize=(12,14))
ax1 = aplpy.FITSFigure(fitsfile, dimensions=[0,1], figure=fig)
ax1.recenter(RA, DEC, width=size, height=size)  # decimal degrees
imdata = fits.getdata(fitsfile)[0,0,:,:]
# ax1.show_colorscale(cmap='fusion', vmin=0.6*np.min(imdata), vmax=0.5*np.max(imdata))
ax1.show_colorscale(cmap='inferno', vmin=0.1*np.min(imdata), vmax=0.1*np.max(imdata), interpolation='nearest')
ax1.add_grid()
ax1.axis_labels.set_xtext(u'$\\mathrm{Right}$'+' '+u'$\\mathrm{Ascension}$' + ' ' +u'$\\mathrm{(J2000)}$')
ax1.axis_labels.set_ytext(u'$\\mathrm{Declination}$'+' '+u'$\\mathrm{(J2000)}$')
ax1.set_tick_labels_format(xformat='hh:mm:ss', yformat='dd:mm')
ax1.set_tick_color('white')
ax1.set_tick_labels_style('latex')
ax1.set_labels_latex(True)
# ax1.show_beam(edgecolor='white', facecolor='none', lw=1)
ax1.show_colorbar()
ax1.colorbar.set_axis_label_text(u'$\\mathrm{Jy/beam}$')

fig.canvas.draw()
plt.show()